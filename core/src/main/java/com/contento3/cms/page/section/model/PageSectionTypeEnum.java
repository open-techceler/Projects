package com.contento3.cms.page.section.model;

public enum PageSectionTypeEnum {

	HEADER,
	FOOTER,
	BODY,
	LEFT_NAVIGATION,
	RIGHT_NAVIGATION,
	META_TAGS,
	CUSTOM
}
