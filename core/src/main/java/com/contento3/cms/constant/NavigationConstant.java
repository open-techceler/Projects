package com.contento3.cms.constant;

public class NavigationConstant {
	public static final String LAYOUT_MANAGER = "Layout Manager";            
	public static final String CONTENT_MANAGER = "Content Manager"; 
	public static final String CONTENT_ART_MGMT = "Article Manager"; 
	public static final String CONTENT_IMG_MGMT = "Image Manager"; 
	public static final String CONTENT_VID_MGMT = "Video Manager";
	
	public static final String TEMPLATE = "Template";          
	public static final String MODULES = "Modules";          
                                                                              
	public static final String SITES = "Sites";                   
	public static final String USER_MANAGER = "User Manager"; 
	public static final String USER_GRP_MGMT = "Group Manager";
	public static final String GLOBAL_CONFIG = "Global Config";
	
	public static final String SECURITY = "Security";
	
	public static final String DOCUMENT_MGMT = "Document Manager";
}
