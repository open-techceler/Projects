package com.contento3.cms.content.dao;

import com.contento3.cms.content.model.AssociatedContentScope;
import com.contento3.common.dao.GenericDao;

public interface AssociatedContentScopeDao extends GenericDao<AssociatedContentScope, Integer> {

}
